# P2PB

I often make small bets on things with my friends. I bet you we'll arrive at the train before 10:15. I bet you I can eat more hot dogs than you can. We even sometimes play credit card roulette to see who pays the bill.

The mission of P2PB (P2P Betting) is simple: create an app where N number of players can bet on anything they like and have terms of the bet be secured through the blockchain.

It's not intended to be a sports-betting or gambling app, but just a platform to serve agreements between parties where there is a medium involved. We can create the same application using PayPal or Venmo, but then we don't have smart contracts to execute the bets. Using a smart contract/Ethereum is amazing in this case because the bets are executed on a decentralized platform and the open-source nature of the platform is very transparent.

## Two Player Betting Game

A two-player game of arbitrary rules will require an additional unbiased player to be the judge. In sports-betting, the For p2p betting, you need p+1 players because there needs to be a judge. In this p2p betting game, the judge will act to verify the bets. Both players agree by the judges ruling.

Certain bets such as sporting events can be validated by checking if multiple sources collaborate, but this wont hold true for all bets.  For prop bets, you need a judge to avoid future disputes.

Let's say players dont want a judge. They can just agree by themselves, because they're best friends or what not. Pretty easy to see how this will result in the prop bet being stalled in the future if one party doesn't agree with the decision and refuses to settle the bet. Although this is going to be an edge case, these cases lead to a big issues for the system.

### Objective V Subjective Bets

Objective bets are best that can be validated because there is an official record, like who wins superbowl or what was Clayton Kershaw's ERA in 2017. Subjective bets are ones decided by opinion, like whos the better ice skater or whose t-shirt is greener. Objective bets require a valid source and subjective bets require a judge. In relation to the platform, we cannot fathom every single objective bet, so we can only host certain verifiable events as objective bets. All other betting will require a judge. It will also to call a judge into objective bets in case you don't trust the platform's results.

### The Formula (pseudocode)

pr: possible results
e: event
a, b: players
k: bookie
m(e): winning if e occurs
s: deal status
v: vig (spectator tip)
g: gas

pr = [e1, e2]

if s == "agreed":
  a → k, m(e2)+g+v
  ‎b → k, m(e1)+g+v

if s != "cancelled":

  if e1:
    k → a, m(e1) + m(e2)

  if e2:
    k → b, m(e2) + m(e1)

else:

  k → a, m(e2)
  ‎k → b, m(e1)

### Pre-Launch Ideas / Brainstorm

- leftover gas would roll over to next transaction, likewise underspent gas
- k would need to be restricted on taking out any funds that are in escrow or gas for those funds, escrow must be locked from withdrawals but not from player payouts
- ‎only owner can specify accounts which are meant for withdraws, so admins cant steal all the money to their own wallet
- ‎there also needs to be a restriction to prevent infinte loop from draining all bets?
- ‎need to keep gas balances possibly off block
- ‎to be pure p2p betting, people need to be able to propose any bet on any event, and hire someone to be a judge (platform)
- ‎possibly there could be more than one judge
- ‎if the judge never makes a decision, the game would stall, the platform would always need to be included as a judge
- ‎after a time limit, the platform can become a judge, so the platform is like a backup judge
- ‎off platform judges would get incentivized as well
- ‎odds are completely decided by players, completely disregard vegas or whatevers
- ‎sportscrypt has the option of in-game betting which adds more bets, this can be accomplished through any prop bet
- ‎do all contracts require a counterparty? is it really necessary to even make this check?
- ‎when is gas paid exactly?  who pays the gas when the contract is initiated? 


