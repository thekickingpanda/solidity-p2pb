pragma solidity ^0.4.18;

contract P2PB {

    address private _master;
    mapping (address => bool) private _owners;
    mapping (address => uint) private _balances;
    mapping (uint => BetTwo) private bets;
    uint private _escrow;

    // sets the master of the contract on contract initiation
    function P2PB() public {
        _master = msg.sender;
        _owners[parseAddr(msg.sender)] = true;
    }

    // checks if user is owner
    modifier isOwner() {
        require(_master == msg.sender || _owners[msg.sender]);
        _;
    }

    // checks if sender is judge or owner
    modifier isJudge(uint8 matchId) {
        require(_master == msg.sender || _owners[msg.sender] || msg.sender == bets[matchId].judgeAddr);
        _;
    }

    // checks if user is master
    modifier isMaster() {
        require(_master == msg.sender);
        _;
    }

    // adds a new owner (TODO limit number of admins)
    function addOwner(string _addr)
    isOwner {
        _owners[parseAddr(_addr)] = true;
    }

    function removeOwner(string _addr)
    isOwner {
        if (msg.sender != _master)
        _owners[parseAddr(_addr)] = false;
    }


    // EVENTS

    event Transfer(address frAddr, address toAddr, uint _value);

    // 2 PLAYER GAME

    enum Status {
        IN_PROGRESS,
        COMPLETED,
        EXPIRED,
        CANCELLED,
        ERROR
    }

    enum BetType {
        OBJECTIVE,
        SUBJECTIVE
    }

    struct BetTwo {
        uint8 eventId;
        BetType type;
        uint64 timestamp;
        Status status;
        address initAddr;
        address counterAddr;
        address judgeAddr;
        uint initWagerWei;
        uint counterWagerWei;
        uint judgeWei;
        uint gasWei;
        uint8 winningPlayer;
    }

    function createBetTwo(Order o) private constant returns(BetTwo b) {
        b.eventId = o.eventId;
        b.type = o.isObjective;
        if (b.timestamp == 0) {
            assert(block.timestamp > 0);
            b.timestamp = uint64(block.timestamp);
        }
        b.status = status.IN_PROGRESS;
        b.initAddr = msg.sender;
        b.counterAddr = o.counter;
        if (o.judge != 0) {
            b.judgeAddr = o.judge;
        } else {
            b.judgeAddr = _master;
        }
        b.initWagerWei = o.initWei;
        b.counterWagerWei = o.counterWei;
        b.judgeWei = judgeWei * players;
        b.gasWei = o.gasWei * players;
    }

    // Bet
    struct Order {
        uint orderHash;
        uint eventId;
        uint8 isObjective;
        address counter;
        address judge;
        uint initWei;
        uint counterWei;
        uint judgeWei;
        uint gasWei;
    }

    function parseOrder(uint[7] memory rawOrder) private constant returns(Order memory o) {
        // takes raw order list of ints and returns and Order class
        o.orderHash = uint(keccak256(this, rawOrder));
        o.eventId = rawOrder[0];
        o.isObjective = rawOrder[1] == 1;
        o.counter = address(rawOrder[2] & 0x00ffffffffffffffffffffffffffffffffffffffff);
        o.judge = address(rawOrder[3] & 0x00ffffffffffffffffffffffffffffffffffffffff);
        o.initWei = rawOrder[4];
        o.counterWei = rawOrder[5];
        o.judgeWei = rawOrder[6];
        o.gasWei = rawOrder[7];
    }

    function validateOrder(Order memory o) private pure returns(string) {
        // just makes sure values arent ridulous (> 100 eth)
        if (o.initWei > 100000000000000000000) return "bet wei too high";
        if (o.counterWei > 100000000000000000000) return "counter wei too high";
        if (o.judgeWei > 100000000000000000000) return "judge wei too high";
        if (bets[o.eventId].eventId > 0) return "eventId already in use";
        return "";
    }

    function p2pbBet (uint8 players, uint[7] order) external {
        // forms a bet between 2 players
        if (players != 2)
            return;

        Order memory o = parseOrder(order);

        if (validateOrder(o) != "") {
            // TODO do something with validateOrder(o) error message
            return;
        }

        priceInit = o.initWei + o.judgeWei + o.gasWei;
        priceCounter = o.counterWei + o.judgeWei + o.gasWei;
        if(balances[msg.sender] < priceInit)
            return "initiator of Bet has insufficient funds";
        if(balances[o.counter] < priceCounter)
            return "counterparty of Bet has insufficient funds";

        bets[o.eventId] = createBetTwo(o.order);

        // send wei from players to contract for _escrow
        balances[msg.sender] -= priceInit;
        balances[o.counter] -= priceCounter;
        _escrow += (priceInit + priceCounter - (o.gasWei * 2));
        s.status = status.IN_PROGRESS;
    }

    function p2pbSettleBetTwo (uint matchId, bool initIsWinner)
    checkJudge external {
        // settles a bet between 2 players
        var m = bets[matchId];

        if (m.status != Status.IN_PROGRESS)
            return "This bet is no longer status IN_PROGRESS";

        if (m.winningPlayer > 0)
            return "CRITICAL BUG: winning player already assigned";

        uint winnerAddr = (initIsWinner) ? m.initAddr : m.counterAddr;
        uint winningAmt = m.initWagerWei + m.counterWagerWei;

        uint messageHash = uint(keccak256(this, matchId, winningAmt));
        address signer = ecrecover(keccak256("\x19Ethereum Signed Message:\n32", messageHash), v, r, s);

        // send eth from master to player
        if(this.balance < winningAmt)
            return "CRITICAL BUG: master has insufficient funds to settle";
        balances[winnerAddr] += winningAmt;

        if(this.balance < m.judgeWei)
            return "CRITICAL BUG: unable to pay judge";
        balances[m.judgeAddr] += m.judgeWei;

        _escrow -= (winningAmt + m.judgeWei);

        m.winningPlayer = (initIsWinner) ? 1 : 2;
        m.status = status.COMPLETED;
    }

    function p2pbCancelBet (uint matchId)
    isOwner {
        // cancels a bet between 2 players (only admin can do this)
        var m = bets[matchId];
        uint8 players = 2;

        if (m.status != Status.IN_PROGRESS)
            return "This bet is no longer status IN_PROGRESS";

        uint initPrice = m.initWagerWei + (m.judgeWei / players);
        uint counterPrice = m.counterWagerWei + (m.judgeWei / players);

        // refund eth from contract back to players
        if(this.balance < (initPrice + counterPrice))
            return "CRITICAL BUG: master has insufficient funds to refund";
        balances[m.initAddr] += initPrice;
        balances[m.counterAddr] += counterPrice;
        _escrow -= (initPrice + counterPrice);
        
        m.status = status.CANCELLED;
    }

    // master withdrawal
    function p2pb(uint amt)
    isMaster {
        pullable = this.balance - _escrow;
        if (amt > pullable)
            return "insufficient funds to pull amt";
        balances[_master] += amt;
    }

    // external views
    function checkEscrow()
    isOwner {
        return _escrow;
    }

    function checkPullable()
    isMaster {
        return this.balance - _escrow;
    }

    function getBetInfo(uint matchId) external view returns(BetTwo) {
        return bets[matchId];
    }
}