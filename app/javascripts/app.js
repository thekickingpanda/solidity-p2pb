import "../stylesheets/app.css";
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

// Import contract artifacts and turn them into usable abstractions.
import contract_artifacts from '../../build/contracts/P2PB.json'
var P2PB = contract(contract_artifacts);

var accounts;
var account;

window.App = {
  start: function() {
    var self = this;

    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      account = accounts[0];

      self.refreshBalance();
    });
  },

  setStatus: function(message) {
    var status = document.getElementById("status");
    status.innerHTML = message;
  },

  refreshBalance: function() {
    var self = this;

    var address = document.getElementById("sender").value;
    web3.eth.getBalance(address, 'latest', function(err, result) {
      if (err != null) {
        console.error("Error while retrieving the balance for address[" + address + "]: " + err);
      }

      var balance = Number(web3.fromWei(result, "ether"));
      console.debug("Balance for address[" + address + "]=" + balance);

      var balance_element = document.getElementById("balSender");
      balance_element.innerHTML = balance.valueOf();
    })

    address = document.getElementById("receiver").value;
    web3.eth.getBalance(address, 'latest', function(err, result) {
      if (err != null) {
        console.error("Error while retrieving the balance for address[" + address + "]: " + err);
      }

      var balance = Number(web3.fromWei(result, "ether"));
      console.debug("Balance for address[" + address + "]=" + balance);

      var balance_element = document.getElementById("balReceiver");
      balance_element.innerHTML = balance.valueOf();
    })
  },

  sendCoin: function() {
    var self = this;

    var amount = parseInt(document.getElementById("amount").value);
    var sender = document.getElementById("sender").value;
    var receiver = document.getElementById("receiver").value;

    var send = web3.eth.sendTransaction({from:sender,to:receiver, value:web3.toWei(amount, "ether")});
    self.setStatus(send.toString());

    // web3.eth.sendTransaction(sender, receiver, amount, function(err, result) {
    //   if (err != null) {
    //     console.error("Error sending " + err);
    //     self.setStatus("Error sending coin; see log.");
    //   } else {
    //     console.error("Send successful " + result);
    //     self.setStatus("Transaction complete!");
    //     self.refreshBalance();
    //   };
    // })

    // this.setStatus("Initiating transaction... (please wait)");
    //
    // var ppb_instance;
    // P2PB.deployed().then(function(instance) {
    //   ppb_instance = instance;
    //   return ppb_instance.sendCoin(receiver, amount, {from: sender})
    // }).then(function() {
    //   self.setStatus("Transaction complete!");
    //   self.refreshBalance();
    // }).catch(function(e) {
    //   console.log(e);
    //   self.setStatus("Error sending coin; see log.");
    // });
  }
};

window.addEventListener('load', function() {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://127.0.0.1:9545. Consider switching to Metamask for development.");
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:9545"));
  }

  App.start();
});

